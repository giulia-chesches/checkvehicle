﻿using CheckVehicle.Data.Models;
using System.Threading.Tasks;

namespace CheckVehicle.Users.Interfaces
{
    public interface IUserService
    {
        Task CreateUser(User user);
        Task<User> FindByEmail(string email);
    }
}
