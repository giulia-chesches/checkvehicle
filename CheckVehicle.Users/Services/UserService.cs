﻿using CheckVehicle.Data.Models;
using CheckVehicle.Repo.Interfaces;
using CheckVehicle.Users.Interfaces;
using System.Threading.Tasks;

namespace CheckVehicle.Users.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task CreateUser(User user)
        {
            await _userRepository.AddAsync(user);
        }

        public async Task<User> FindByEmail(string email)
        {
            return await _userRepository.GetByEmail(email);
        }
    }
}
