﻿using CheckVehicle.Web.Resources.UsersResources;
using FluentValidation;

namespace CheckVehicle.Web.Validators.UsersValidators
{
    public class SaveUserValidator : AbstractValidator<UserRequest>
    {
        public SaveUserValidator()
        {
            RuleFor(x => x.Email).NotEmpty().WithMessage("Email address is required")
                .EmailAddress().WithMessage("Email address is invalid");

            RuleFor(x => x.PhoneNumber)
                .MatchPhoneNumberRule()
                .WithMessage("Please provide valid phone number");
        }
    }
}
