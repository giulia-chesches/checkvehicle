﻿using AutoMapper;
using CheckVehicle.Data.Models;
using CheckVehicle.Web.Resources.UsersResources;

namespace CheckVehicle.Web.Mappers.UsersMappers
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserRequest, User>();
        }
    }
}
