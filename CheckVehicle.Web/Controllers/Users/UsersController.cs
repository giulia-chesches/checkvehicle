﻿using AutoMapper;
using CheckVehicle.Data.Models;
using CheckVehicle.Users.Interfaces;
using CheckVehicle.Web.Resources.UsersResources;
using CheckVehicle.Web.Validators.UsersValidators;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using BC = BCrypt.Net.BCrypt;

namespace CheckVehicle.Web.Controllers.Users
{
    [Route("[controller]/[action]")]
    [ApiController]
   // [Authorize]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public UsersController(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<ActionResult> Create([FromBody] UserRequest userResource)
        {
            var validator = new SaveUserValidator();
            var validationResult = await validator.ValidateAsync(userResource);

            if (!validationResult.IsValid)
                return BadRequest(string.Join(";\n", validationResult.Errors.Select(x => x.ErrorMessage)));

            var newUser = _mapper.Map<User>(userResource);
            newUser.HashPassword = BC.HashPassword(userResource.Password);

            try
            {
                await _userService.CreateUser(newUser);
            }
            catch(Exception ex)
            {
                return BadRequest();
            }

            return Ok();
        }
    }
}
