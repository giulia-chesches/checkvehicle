﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CheckVehicle.Repo.Interfaces
{
    public interface IBaseRepository<TEntity> where TEntity : class
    {
        Task AddAsync(TEntity entity);
        Task<IEnumerable<TEntity>> GetAllAsync();
        ValueTask<TEntity> GetByIdAsync(int id);
        void Remove(TEntity entity);
        IQueryable<TEntity> GetAll();
    }
}
