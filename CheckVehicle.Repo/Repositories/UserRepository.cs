﻿using CheckVehicle.Data.Context;
using CheckVehicle.Data.Models;
using CheckVehicle.Repo.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace CheckVehicle.Repo.Repositories
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(ApplicationContext context) 
            : base(context)
        {
        }
        
        public Task<User> GetByEmail(string email)
        {
            return GetAll().FirstOrDefaultAsync(x => x.Email == email);
        }
    }
}
