﻿using CheckVehicle.Data.Context;
using CheckVehicle.Repo.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CheckVehicle.Repo.Repositories
{
    public abstract class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class
    {
        protected readonly ApplicationContext Context;
        private readonly DbSet<TEntity> DbSet;

        public BaseRepository(ApplicationContext context)
        {
            Context = context;
            DbSet = Context.Set<TEntity>();
        }

        public async Task AddAsync(TEntity entity)
        {
            DbSet.Add(entity);
            await Context.SaveChangesAsync();
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return await DbSet.ToListAsync();
        }

        public IQueryable<TEntity> GetAll()
        {
            return DbSet;
        }

        public ValueTask<TEntity> GetByIdAsync(int id)
        {
            return DbSet.FindAsync(id);
        }

        public async void Remove(TEntity entity)
        {
            DbSet.Remove(entity);
            await Context.SaveChangesAsync();
        }
    }
}
