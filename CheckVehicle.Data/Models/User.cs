﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CheckVehicle.Data.Models
{
    public class User
    {
        public Guid Id { get; set; }
        [Required(AllowEmptyStrings = false)]
        [StringLength(50)]
        public string FirstName { get; set; }
        [Required(AllowEmptyStrings = false)]
        [StringLength(50)]
        public string LastName { get; set; }

        [Required(AllowEmptyStrings = false)]
        [StringLength(300)]
        public string Email { get; set; }

        [StringLength(50)]
        public string PhoneNumber { get; set; }

        [StringLength(200)]
        [Required(AllowEmptyStrings = false)]
        public string HashPassword { get; set; }
    }
}
