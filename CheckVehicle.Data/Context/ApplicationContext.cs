﻿using CheckVehicle.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace CheckVehicle.Data.Context
{
    public class ApplicationContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            :base(options)
        {
            this.ChangeTracker.LazyLoadingEnabled = false;
        }
    }
}
